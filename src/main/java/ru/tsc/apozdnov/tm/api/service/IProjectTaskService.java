package ru.tsc.apozdnov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.model.Project;

public interface IProjectTaskService {

    void bindTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    void unbindTaskFromProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    void removeProjectById(@NotNull String userId, @NotNull String projectId);

}

package ru.tsc.apozdnov.tm.task;

import ru.tsc.apozdnov.tm.component.Server;

import java.net.Socket;

public abstract class AbstractServerSocketTask extends AbstractServerTask {

    protected final Socket socket;

    public AbstractServerSocketTask(final Server server, final Socket socket) {
        super(server);
        this.socket = socket;
    }

}

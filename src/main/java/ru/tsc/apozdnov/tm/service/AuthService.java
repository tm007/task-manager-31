package ru.tsc.apozdnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.service.IAuthService;
import ru.tsc.apozdnov.tm.api.service.IPropertyService;
import ru.tsc.apozdnov.tm.api.service.IUserService;
import ru.tsc.apozdnov.tm.enumerated.RoleType;
import ru.tsc.apozdnov.tm.exception.user.*;
import ru.tsc.apozdnov.tm.model.User;
import ru.tsc.apozdnov.tm.util.HashUtil;

import javax.naming.AuthenticationException;
import java.util.Arrays;
import java.util.Optional;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private String userId;

    public AuthService(final IUserService userService, final IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public void registry(String login, String password, String email) {
        userService.create(login, password, email);
    }

    @Override
    public void login(@NotNull String login, @NotNull String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = Optional.ofNullable(userService.findOneByLogin(login)).orElseThrow(AccessDeniedException::new);
        if (user.isLocked()) throw new PermissionException();
        @Nullable String hash = HashUtil.salt(propertyService, password);
        if (!hash.equals(user.getPasswordHash())) throw new LoginOrPasswordIncorrectException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        return userService.findOneById(userId);
    }

    @Nullable
    @Override
    public void checkRoles(final RoleType[] roleTypes) {
        if (roleTypes == null) return;
        @NotNull final User user = getUser();
        @Nullable final RoleType role = user.getRoleType();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roleTypes).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
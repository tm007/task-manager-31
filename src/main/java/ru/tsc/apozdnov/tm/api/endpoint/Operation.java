package ru.tsc.apozdnov.tm.api.endpoint;

import ru.tsc.apozdnov.tm.dto.request.AbstractRequest;
import ru.tsc.apozdnov.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}

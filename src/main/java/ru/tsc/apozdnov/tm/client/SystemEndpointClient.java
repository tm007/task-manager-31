package ru.tsc.apozdnov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.apozdnov.tm.dto.request.ServerAboutRequest;
import ru.tsc.apozdnov.tm.dto.request.ServerVersionRequest;
import ru.tsc.apozdnov.tm.dto.response.ServerAboutResponse;
import ru.tsc.apozdnov.tm.dto.response.ServerVersionResponse;

public class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @SneakyThrows
    public static void main(String[] args) {
        final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        final ServerAboutResponse about = client.getAbout(new ServerAboutRequest());
        final ServerVersionResponse version = client.getVersion(new ServerVersionRequest());
        System.out.println("version: \n" + version.getVersion());
        System.out.println("about: \n" + about.getName() + "   " + about.getEmail());
        client.disconnect();
    }

    @Override
    @SneakyThrows
    public @NotNull ServerAboutResponse getAbout(@NotNull final ServerAboutRequest serverAboutRequest) {
        return (ServerAboutResponse) call(serverAboutRequest);
    }

    @Override
    @SneakyThrows
    public @NotNull ServerVersionResponse getVersion(@NotNull final ServerVersionRequest serverVersionRequest) {
        return (ServerVersionResponse) call(serverVersionRequest);
    }

}

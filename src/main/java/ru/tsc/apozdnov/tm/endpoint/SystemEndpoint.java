package ru.tsc.apozdnov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.apozdnov.tm.api.service.IPropertyService;
import ru.tsc.apozdnov.tm.api.service.IServiceLocator;
import ru.tsc.apozdnov.tm.dto.request.ServerAboutRequest;
import ru.tsc.apozdnov.tm.dto.request.ServerVersionRequest;
import ru.tsc.apozdnov.tm.dto.response.ServerAboutResponse;
import ru.tsc.apozdnov.tm.dto.response.ServerVersionResponse;

public final class SystemEndpoint implements ISystemEndpoint {

    @NotNull
    private IServiceLocator serviceLocator;

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NotNull
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest serverAboutRequest) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerAboutResponse serverAboutResponse = new ServerAboutResponse();
        serverAboutResponse.setEmail(propertyService.getAuthorEmail());
        serverAboutResponse.setName(propertyService.getAuthorName());
        return serverAboutResponse;
    }

    @Override
    @NotNull
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest serverVersionRequest) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerVersionResponse serverVersionResponse = new ServerVersionResponse();
        serverVersionResponse.setVersion(propertyService.getApplicationVersion());
        return serverVersionResponse;
    }

}

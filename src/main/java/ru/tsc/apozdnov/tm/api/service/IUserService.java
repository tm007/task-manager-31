package ru.tsc.apozdnov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.enumerated.RoleType;
import ru.tsc.apozdnov.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String password);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull String email);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull RoleType role);

    @NotNull
    User findOneById(@NotNull String id);

    @Nullable
    User findOneByLogin(@NotNull String login);

    @Nullable
    User findOneByEmail(@NotNull String email);

    @NotNull
    User removeByLogin(@NotNull String login);

    @NotNull
    User removeByEmail(@NotNull String email);

    @NotNull
    User setPassword(@NotNull String id, @NotNull String password);

    @NotNull
    User userUpdate(@NotNull String id, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName);

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

    void lockUserByLogin(@NotNull String login);

    void unlockUserByLogin(@NotNull String login);

}

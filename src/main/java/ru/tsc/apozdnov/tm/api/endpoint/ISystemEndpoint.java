package ru.tsc.apozdnov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.dto.request.ServerAboutRequest;
import ru.tsc.apozdnov.tm.dto.request.ServerVersionRequest;
import ru.tsc.apozdnov.tm.dto.response.ServerAboutResponse;
import ru.tsc.apozdnov.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull ServerAboutResponse getAbout(@NotNull ServerAboutRequest serverAboutRequest);

    @NotNull ServerVersionResponse getVersion(@NotNull ServerVersionRequest serverVersionRequest);

}

package ru.tsc.apozdnov.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.endpoint.Operation;
import ru.tsc.apozdnov.tm.api.service.IPropertyService;
import ru.tsc.apozdnov.tm.dto.request.AbstractRequest;
import ru.tsc.apozdnov.tm.dto.response.AbstractResponse;
import ru.tsc.apozdnov.tm.task.AbstractServerTask;
import ru.tsc.apozdnov.tm.task.ServerAcceptTask;
import ru.tsc.apozdnov.tm.task.ServerRequestTask;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class Server {

    private final Bootstrap bootstrap;

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    private final Dispatcher dispatcher = new Dispatcher();

    @Getter
    @Nullable
    private ServerSocket socketServer;

    public Server(final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void submit(@NotNull final AbstractServerTask task) {
        executorService.submit(task);
    }

    @SneakyThrows
    public void start() {
        @NotNull final IPropertyService propertyService = bootstrap.getPropertyService();
        @NotNull final Integer port = propertyService.getServerPort();
        socketServer = new ServerSocket(port);
        submit(new ServerAcceptTask(this));
    }

    @SneakyThrows
    public void stop() {
        if (socketServer == null) return;
        socketServer.close();
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(final Class<RQ> reqClass, final Operation<RQ, RS> operation) {
        dispatcher.registry(reqClass, operation);
    }

    public Object call(final AbstractRequest request) {
        return dispatcher.call(request);
    }

}

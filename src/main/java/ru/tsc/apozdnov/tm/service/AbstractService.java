package ru.tsc.apozdnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.repository.IRepository;
import ru.tsc.apozdnov.tm.api.service.IService;
import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.apozdnov.tm.exception.field.IdEmptyException;
import ru.tsc.apozdnov.tm.exception.field.IndexIncorrectException;
import ru.tsc.apozdnov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    public AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public void clear() {
        repository.clear();
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        return repository.add(models);
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        return repository.set(models);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public boolean existsById(@NotNull final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @NotNull
    @Override
    public M findOneById(@NotNull final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Optional<M> model = Optional.ofNullable(repository.findOneById(id));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @NotNull
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        Optional<M> model = Optional.ofNullable(repository.findOneByIndex(index));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

    @Nullable
    @Override
    public M remove(final M model) {
        if (model == null) return null;
        return repository.remove(model);
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        if (model == null) return null;
        return repository.add(model);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll();
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collection) {
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.removeById(id);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        if (index == null) throw new IndexIncorrectException();
        return repository.removeByIndex(index);
    }

}

package ru.tsc.apozdnov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.command.data.AbstractDataCommand;
import ru.tsc.apozdnov.tm.command.data.backup.DataBackupLoadCommand;
import ru.tsc.apozdnov.tm.command.data.backup.DataBackupSaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    private void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.processCommandTask(DataBackupLoadCommand.NAME, false);
    }

    private void save() {
        bootstrap.processCommandTask(DataBackupSaveCommand.NAME, false);
    }

}